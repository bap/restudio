require 'sinatra'

get '/' do
  @nb_items = "6"
  erb :index
end

get '/:nb' do |nb|
  @nb_items = "#{nb}"
  erb :index
end

not_found do
  erb :"404"
end
